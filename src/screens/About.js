import React, {Fragment} from 'react';
import useScript from '../hooks/useScript';
import {Link} from 'react-router-dom';

const About = (props) => {

	useScript("./js/jquery.min.js");
	useScript("./js/popper.min.js");
	useScript("./js/bootstrap.min.js");
	useScript("./js/alime.bundle.js");
	useScript("./js/default-assets/active.js");

	return (

		<Fragment>


    
    

    
    <section className="breadcrumb-area bg-img bg-overlay jarallax" style={{backgroundImage: `url(img/bg-img/97.jpg)`}}>
        <div className="container h-100">
            <div className="row h-100 align-items-center">
                <div className="col-12">
                    <div className="breadcrumb-content text-center">
                        <h2 className="page-title">About Us</h2>
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb justify-content-center">
                                <li className="breadcrumb-item"><Link to="/"><i className="icon_house_alt"></i> Home</Link></li>
                                <li className="breadcrumb-item active" aria-current="page">About</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    
    <div className="about-us-area section-padding-80-0 clearfix">
        <div className="container">
            <div className="row align-items-center">
                <div className="col-12 col-lg-6">
                    <div className="about-us-content mb-80">
                        <h3 className="wow fadeInUp" data-wow-delay="100ms">We believe that <br />
                                Beauty is in the eye of the beholder...</h3>
                        <div className="line wow fadeInUp" data-wow-delay="200ms"></div>
                        <p className="wow fadeInUp" data-wow-delay="300ms">"...In our daily life we pass by many things, sometimes we give them attention by observing then in a way that our minds are set to and sometimes we don’t even bother to look. We might see and find the beauty in these things using the ways of knowledge such as reason, emotion and languages. When I think of these things I come to conclusion that they must belong to a certain area of knowledge such as science, religion, arts and mathematics. However in order to examine these things we need the ways of knowledge, so after all the areas of knowledge depends on the ways of knowledge.  Most of the times it turns out that not every person on earth looks at the things as others do. As it is said in the claim ‘we see and understand things not as they are but as we are’. I believe there is more than one vision for each thing.</p>
                        <p className="wow fadeInUp" data-wow-delay="400ms">As it is said ‘Beauty is in the eye of the beholder’."</p>
                        <a className="btn alime-btn btn-2 mt-30 wow fadeInUp" data-wow-delay="500ms" href="#">Contact</a>
                    </div>
                </div>
                <div className="col-12 col-lg-6">
                    <div className="about-video-area mb-80 wow fadeInUp" data-wow-delay="100ms">
                        <img src="img/bg-img/95.jpg" />
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
    

    
    <section className="why-choose-us-area bg-gray section-padding-80-0 clearfix">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="section-heading text-center wow fadeInUp" data-wow-delay="100ms">
                        <h2>Why Choose Us</h2>
                    </div>
                </div>
            </div>

            <div className="row">
                
                <div className="col-md-6 col-lg-4">
                    <div className="why-choose-us-content text-center mb-80 wow fadeInUp" style={{height: '250px', width: '400px'}} data-wow-delay="100ms">
                        <div className="chosse-us-icon">
                            <i className="fa fa-film" aria-hidden="true"></i>
                        </div>
                        <h4>Quality Shots</h4>
                        <p>Over the years, we have exceeded our clients' expectations by providing excellent, no nonsense service. </p>
                    </div>
                </div>

                
                <div className="col-md-6 col-lg-4">
                    <div className="why-choose-us-content text-center mb-80 wow fadeInUp" style={{height: '250px', width: '400px'}} data-wow-delay="300ms">
                        <div className="chosse-us-icon">
                            <i className="fa fa-pencil" aria-hidden="true"></i>
                        </div>
                        <h4>Extensive Experience</h4>
                        <p>Photography is more than cameras and lights. Our photographer's experience proved to be the key to great, quality shots. </p>
                    </div>
                </div>

                
                <div className="col-md-6 col-lg-4">
                    <div className="why-choose-us-content text-center mb-80 wow fadeInUp" style={{height: '250px', width: '400px'}} data-wow-delay="500ms">
                        <div className="chosse-us-icon">
                            <i className="fa fa-camera" aria-hidden="true"></i>
                        </div>
                        <h4>Modern Equipments</h4>
                        <p>When it comes to great shots, we only use the best. The Canon EOS-1Dx Mark III is one of the cameras that we use and is something that we are proud of. It is also the top choice of camera for many pros. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    
    <section className="our-team-area section-padding-80-50">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="section-heading text-center wow fadeInUp" data-wow-delay="100ms">
                        <h2>My Team</h2>
                    </div>
                </div>
            </div>

            <div className="row">
                
                <div className="col-md-6 col-xl-3">
                    <div className="team-content-area text-center mb-30 wow fadeInUp" data-wow-delay="100ms">
                        <div className="member-thumb">
                            <img src="img/bg-img/jordi.jpg" />
                        </div>
                        <h5>Angel Muñoz</h5>
                        <span>Photographer</span>
                        <div className="member-social-info">
                            <a href="#"><i className="ti-facebook"></i></a>
                            <a href="#"><i className="ti-twitter-alt"></i></a>
                            <a href="#"><i className="ti-linkedin"></i></a>
                            <a href="#"><i className="ti-pinterest"></i></a>
                        </div>
                    </div>
                </div>

                
                <div className="col-md-6 col-xl-3">
                    <div className="team-content-area text-center mb-30 wow fadeInUp" data-wow-delay="300ms">
                        <div className="member-thumb">
                            <img src="img/bg-img/julio.jpg" />
                        </div>
                        <h5>Julio Gomez</h5>
                        <span>Photographer</span>
                        <div className="member-social-info">
                            <a href="#"><i className="ti-facebook"></i></a>
                            <a href="#"><i className="ti-twitter-alt"></i></a>
                            <a href="#"><i className="ti-linkedin"></i></a>
                            <a href="#"><i className="ti-pinterest"></i></a>
                        </div>
                    </div>
                </div>

                
                <div className="col-md-6 col-xl-3">
                    <div className="team-content-area text-center mb-30 wow fadeInUp" data-wow-delay="500ms">
                        <div className="member-thumb">
                            <img src="img/bg-img/rebecca.jpg" />
                        </div>
                        <h5>Rebecca Linares</h5>
                        <span>Photographer</span>
                        <div className="member-social-info">
                            <a href="#"><i className="ti-facebook"></i></a>
                            <a href="#"><i className="ti-twitter-alt"></i></a>
                            <a href="#"><i className="ti-linkedin"></i></a>
                            <a href="#"><i className="ti-pinterest"></i></a>
                        </div>
                    </div>
                </div>

                
                <div className="col-md-6 col-xl-3">
                    <div className="team-content-area text-center mb-30 wow fadeInUp" data-wow-delay="700ms">
                        <div className="member-thumb">
                            <img src="img/bg-img/nacho.jpg" />
                        </div>
                        <h5>Nacho Vidal</h5>
                        <span>Photographer</span>
                        <div className="member-social-info">
                            <a href="#"><i className="ti-facebook"></i></a>
                            <a href="#"><i className="ti-twitter-alt"></i></a>
                            <a href="#"><i className="ti-linkedin"></i></a>
                            <a href="#"><i className="ti-pinterest"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    
    <div className="follow-area clearfix">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="section-heading text-center">
                        <h2>Follow Instagram</h2>
                        <p>@myriad_69</p>
                    </div>
                </div>
            </div>
        </div>

        
        <div className="instragram-feed-area owl-carousel">
            
            <div className="single-instagram-item">
                <img src="img/bg-img/11.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/12.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/13.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/14.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/15.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/16.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    

    
    <footer className="footer-area">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="footer-content d-flex align-items-center justify-content-between">
                        
                        <div className="copywrite-text">
                            <p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> | Myriad
</p>
                        </div>
                        
                       
                        
                        <div className="social-info">
                            <a href="#"><i className="ti-facebook" aria-hidden="true"></i></a>
                            <a href="#"><i className="ti-twitter-alt" aria-hidden="true"></i></a>
                            <a href="#"><i className="ti-linkedin" aria-hidden="true"></i></a>
                            <a href="#"><i className="ti-pinterest" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


		</Fragment>




	)
}

export default About;