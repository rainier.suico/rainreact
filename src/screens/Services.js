import React, {Fragment} from 'react';
import useScript from '../hooks/useScript';
import {Link} from 'react-router-dom';

const Services = (props) => {

	useScript("./js/jquery.min.js");
	useScript("./js/popper.min.js");
	useScript("./js/bootstrap.min.js");
	useScript("./js/alime.bundle.js");
	useScript("./js/default-assets/active.js");

	return (

	<>

	<section className="breadcrumb-area bg-img bg-overlay jarallax" style={{backgroundImage: `url(img/bg-img/studio.jpg)`}}>
        <div className="container h-100">
            <div className="row h-100 align-items-center">
                <div className="col-12">
                    <div className="breadcrumb-content text-center">
                        <h2 className="page-title">Services</h2>
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb justify-content-center">
                                <li className="breadcrumb-item"><Link to="/"><i className="icon_house_alt"></i> Home</Link></li>
                                <li className="breadcrumb-item active" aria-current="page">Services</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
    

    
    <div className="alime-blog-area section-padding-80-0 mb-70">
        <div className="container">
            <div className="row">

                
                <div className="col-lg-6">
                    <div className="single-post-area wow fadeInUpBig" data-wow-delay="100ms">
                        
                        <a href="#" className="post-thumbnail"><img src="img/bg-img/67.jpg" style={{height: '500px'}} /></a>
                        
                        <a href="#" className="btn post-catagory">Wedding</a>
                        
                        <div className="post-content">
                            <a href="#" className="post-title"> The heart has its reasons which reason knows not. </a>
                        </div>
                    </div>
                </div>

                
                <div className="col-lg-6">
                    <div className="single-post-area wow fadeInUpBig" data-wow-delay="400ms">
                        
                        <a href="#" className="post-thumbnail"><img src="img/bg-img/66.jpg" style={{height: '500px'}} /></a>
                        
                        <a href="#" className="btn post-catagory">Sexy</a>
                        
                        <div className="post-content">
                           
                            <a href="#" className="post-title">Sexiness is all about personality and confidence.</a>
                        </div>
                    </div>
                </div>

                
                <div className=" col-lg-6">
                    <div className="single-post-area wow fadeInUpBig" data-wow-delay="700ms">
                        
                        <a href="#" className="post-thumbnail"><img src="img/bg-img/1.jpg" style={{height: '500px'}} /></a>
                        
                        <a href="#" className="btn post-catagory">Portrait</a>
                        
                        <div className="post-content">
                            
                            <a href="#" className="post-title">Beauty begins underneath.</a>
                        </div>
                    </div>
                </div>

                
                <div className=" col-lg-6">
                    <div className="single-post-area wow fadeInUpBig" data-wow-delay="100ms">
                        
                        <a href="#" className="post-thumbnail"><img src="img/bg-img/106.jpg" style={{height: '500px'}} /></a>
                        
                        <a href="#" className="btn post-catagory">Nature</a>
                        
                        <div className="post-content">
                            <a href="#" className="post-title">Nature is the art of God.</a>
                        </div>
                    </div>
                </div>

                <div className="hero-btn-group mx-auto mt-5" data-animation="bounceInDown" data-delay="100ms">
                                    <a href="mailto:myriad_69@gmail.com" className="btn alime-btn mb-3 mb-sm-0 mr-4 " style={{color: 'grey'}}>Get a Quote</a>
                                   
                </div>

              

            </div>
        </div>
    </div>
    

     
    <div className="follow-area clearfix">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="section-heading text-center">
                        <h2>Follow Instagram</h2>
                        <p>@myriad_69</p>
                    </div>
                </div>
            </div>
        </div>

        
        <div className="instragram-feed-area owl-carousel">
            
            <div className="single-instagram-item">
                <img src="img/bg-img/11.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/12.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/13.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/14.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/15.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/16.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    

    
     <footer className="footer-area">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="footer-content d-flex align-items-center justify-content-between">
                        
                        <div className="copywrite-text">
                            <p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> | Myriad
</p>
                        </div>
                        
                        
                        
                        <div className="social-info">
                            <a href="#"><i className="ti-facebook" aria-hidden="true"></i></a>
                            <a href="#"><i className="ti-twitter-alt" aria-hidden="true"></i></a>
                            <a href="#"><i className="ti-linkedin" aria-hidden="true"></i></a>
                            <a href="#"><i className="ti-pinterest" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>

	</>


	)

}



export default Services;