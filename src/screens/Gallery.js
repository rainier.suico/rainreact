import React, {Fragment} from 'react';
import useScript from '../hooks/useScript';
import {Link} from 'react-router-dom';

const Gallery = (props) => {

	useScript("./js/jquery.min.js");
	useScript("./js/popper.min.js");
	useScript("./js/bootstrap.min.js");
	useScript("./js/alime.bundle.js");
	useScript("./js/default-assets/active.js");

	return (

	<>

	<section className="breadcrumb-area bg-img bg-overlay jarallax" style={{backgroundImage: `url(img/bg-img/96.jpg)`}}>
        <div className="container h-100">
            <div className="row h-100 align-items-center">
                <div className="col-12">
                    <div className="breadcrumb-content text-center">
                        <h2 className="page-title">Gallery</h2>
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb justify-content-center">
                                <li className="breadcrumb-item"><Link to="/"><i className="icon_house_alt"></i> Home</Link></li>
                                <li className="breadcrumb-item active" aria-current="page">Gallery</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    <div className="alime-portfolio-area section-padding-80 clearfix">
        <div className="container-fluid">
            <div className="row">
                <div className="col-12">
                    
                    <div className="alime-projects-menu wow fadeInUp" data-wow-delay="100ms">
                        <div className="portfolio-menu text-center">
                            <button className="btn active" data-filter="*">All</button>
                            <button className="btn" data-filter=".wedding">Wedding</button>
                            <button className="btn" data-filter=".sexy">Sexy</button>
                            <button className="btn" data-filter=".portrait">Portrait</button>
                            <button className="btn" data-filter=".nature">Nature</button>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row alime-portfolio">
                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item nature mb-30 wow fadeInUp" data-wow-delay="100ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/120.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/120.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item sexy mb-30 wow fadeInUp" data-wow-delay="300ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/98.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/98.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item wedding mb-30 wow fadeInUp" data-wow-delay="500ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/69.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/69.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item portrait mb-30 wow fadeInUp" data-wow-delay="700ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/100.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/100.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item nature mb-30 wow fadeInUp" data-wow-delay="100ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/106.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/106.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item portrait mb-30 wow fadeInUp" data-wow-delay="300ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/125.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/125.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item wedding mb-30 wow fadeInUp" data-wow-delay="500ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/70.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/70.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item sexy mb-30 wow fadeInUp" data-wow-delay="700ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/118.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/118.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item portrait mb-30 wow fadeInUp" data-wow-delay="100ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/117.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/117.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item nature mb-30 wow fadeInUp" data-wow-delay="300ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/108.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/108.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item sexy mb-30 wow fadeInUp" data-wow-delay="500ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/114.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/114.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                
                <div className="col-12 col-sm-6 col-lg-3 single_gallery_item wedding mb-30 wow fadeInUp" data-wow-delay="700ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/68.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/68.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                <div className="col-12 col-sm-6 col-lg-3 item hidden-item hidden single_gallery_item wedding mb-30 wow fadeInUp" data-wow-delay="700ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/67.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/68.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                <div className="col-12 col-sm-6 col-lg-3 item hidden-item hidden single_gallery_item sexy mb-30 wow fadeInUp" data-wow-delay="700ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/110.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/68.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                <div className="col-12 col-sm-6 col-lg-3  item hidden-item hidden single_gallery_item nature mb-30 wow fadeInUp" data-wow-delay="700ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/109.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/68.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>

                <div className="col-12 col-sm-6 col-lg-3 item hidden-item hidden single_gallery_item portrait mb-30 wow fadeInUp" data-wow-delay="700ms">
                    <div className="single-portfolio-content">
                        <img src="img/bg-img/112.jpg" />
                        <div className="hover-content">
                            <a href="img/bg-img/68.jpg" className="portfolio-img">+</a>
                        </div>
                    </div>
                </div>


            </div>

            <div className="row">
                <div className="col-12 text-center wow fadeInUp" data-wow-delay="800ms">
                    <a href="/book" className="btn alime-btn btn-2 mt-15">Book Now</a>
                </div>
            </div>

        </div>
    </div>
    

    
    <div className="follow-area clearfix">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="section-heading text-center">
                        <h2>Follow Instagram</h2>
                        <p>@myriad_69</p>
                    </div>
                </div>
            </div>
        </div>

        
        <div className="instragram-feed-area owl-carousel">
            
            <div className="single-instagram-item">
                <img src="img/bg-img/11.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/12.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/13.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/14.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/15.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
            
            <div className="single-instagram-item">
                <img src="img/bg-img/16.jpg" />
                <div className="instagram-hover-content text-center d-flex align-items-center justify-content-center">
                    <a href="#">
                        <i className="ti-instagram" aria-hidden="true"></i>
                        <span>myriad_69</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    

    
    <footer className="footer-area">
        <div className="container">
            <div className="row">
                <div className="col-12">
                    <div className="footer-content d-flex align-items-center justify-content-between">
                        
                        <div className="copywrite-text">
                            <p>
Copyright &copy;<script>document.write(new Date().getFullYear());</script> | Myriad
</p>
                        </div>
                        
                       
                        
                        <div className="social-info">
                            <a href="#"><i className="ti-facebook" aria-hidden="true"></i></a>
                            <a href="#"><i className="ti-twitter-alt" aria-hidden="true"></i></a>
                            <a href="#"><i className="ti-linkedin" aria-hidden="true"></i></a>
                            <a href="#"><i className="ti-pinterest" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>



	</>


	)

}

export default Gallery;