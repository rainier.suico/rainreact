import React from 'react';

import Home from './screens/Home';
import About from './screens/About';
import Gallery from './screens/Gallery';
import Services from './screens/Services';
import Book from './screens/Book';
import Register from './screens/Register';
import Login from './screens/Login';

import AuthState from './context/auth/AuthState';

import Navbar from './components/Navbar';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

function App() {
  return (
    <>
      <AuthState>
        <Router>
          <Navbar />
          <Switch>
            <Route path="/" exact component={Home} />
          
            <Route path="/about" exact component={About} />
            

            <Route path="/gallery" exact component={Gallery} />
             

            <Route path="/services" exact component={Services} />


            <Route path="/book" exact component={Book} />
     

            <Route path="/register" exact component={Register} />
   

            <Route path="/login" exact component={Login} />
  

          </Switch>
        </Router>
      </AuthState>
    </>
    
    
  );
}

export default App;
